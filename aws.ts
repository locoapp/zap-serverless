import AWS = require("aws-sdk");
AWS.config.update({ region: "ap-south-1" });
let dynamoClient = new AWS.DynamoDB();
let converter = AWS.DynamoDB.Converter;
export { dynamoClient as dynamodb, converter };
