export interface Body {
	name: string;
	when: string;
	content: Content;
	target_mode: string;
	where: {
		[common_profile_properties: string]: Properties;
	};
	devices: string[];
	uid: string;
}

export interface Content {
	title: string;
	subject: string;
	body: string;
	sender_name: string;
	platform_specific: {
		[platform: string]: Platform;
	};
}

export interface Platform {
	type: string;
	title: string;
	message: string;
}

export interface Properties {
	profile_fields: ProfileFields[];
}

export interface ProfileFields {
	name: string;
	operator: string;
	value: string;
}

export interface StopBody {
	id: string;
}
