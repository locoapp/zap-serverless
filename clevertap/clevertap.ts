import axios from "axios";
import { dynamodb } from "../aws";
import httpStatus = require("../utils/httpStatus");
import contentType = require("../utils/contentType");
import error = require("../utils/error");
import { getTableName } from "../tableNames";
import config = require("../utils/config");

export default class CleverTap {
	private account_id: string;
	private account_passcode: string;
	actions: object = {
		CREATE: "create",
		STOP: "stop"
	};
	slack_webhook_url: string;

	constructor(account_id: string, account_passcode: string) {
		this.account_id = account_id;
		this.account_passcode = account_passcode;
		const env = process.env["STAGE"] || "dev";
		this.slack_webhook_url =
			env === "dev"
				? config.slack_webhook_url.devURL
				: config.slack_webhook_url.prodURL;
	}
	notValidAction(action) {
		return this.actions[action] === undefined;
	}
	apiEndpoint(action) {
		let actionString: string = this.actions[action];
		return `https://api.clevertap.com/1/targets/${actionString}.json`;
	}
	private options(action, body) {
		return {
			method: "post",
			url: this.apiEndpoint(action),
			data: body,
			headers: {
				"X-CleverTap-Account-Id": this.account_id,
				"X-CleverTap-Passcode": this.account_passcode,
				"Content-Type": "application/json"
			}
		};
	}

	async saveToDb(body, clevertap_response) {
		let table = getTableName("clevertap_campaigns");
		const params = {
			Item: {
				id: {
					S: `${clevertap_response.id}`
				},
				title: {
					S: body.content.title
				},
				message: {
					S: body.content.body
				},
				time: {
					S: body.when
				},
				language: {
					S: body.name.split("-")[0] || body.name
				},
				name: {
					S: body.name
				},
				created_at: {
					S: new Date().toLocaleString()
				},
				uid: {
					S: body.uid
				}
			},
			TableName: table
		};
		let data = await dynamodb.putItem(params).promise();
		return data;
	}

	static createResponse(statusCode, headers, message) {
		return {
			statusCode: statusCode,
			body: JSON.stringify(message),
			headers: headers
		};
	}
	createSlackPayload(title, fields) {
		let payload = [
			{
				color: "#87d068",
				title: "Contest Details",
				fields: fields
			}
		];

		return {
			text: `<!channel> campaign ${title} successfully Created!`,
			attachments: payload
		};
	}
	async sendSlackMessage(body) {
		let fields = [];
		fields.push({
			title: "Name",
			value: body.name,
			short: true
		});
		fields.push({
			title: "when",
			value: body.when,
			short: true
		});
		fields.push({
			title: "title",
			value: body.content.title,
			short: true
		});
		fields.push({
			title: "body",
			value: body.content.body || body.content.subject,
			short: true
		});
		fields.push({
			title: "profile_fields",
			value: JSON.stringify(
				body.where.common_profile_properties.profile_fields
			),
			short: true
		});
		let payload = this.createSlackPayload(body.name, fields);
		let response = axios.post(this.slack_webhook_url, payload);
	}

	async Targets(action, body) {
		if (this.notValidAction(action)) {
			return CleverTap.createResponse(
				httpStatus.NOT_VALID,
				contentType.ALL,
				error.getNotValidError()
			);
		}
		let response = await axios(this.options(action, body));
		if (response.data && response.status == 200) {
			try {
				let data = await this.saveToDb(body, response.data);
				response.data["message"] = "Campaign Created and saved in db";
			} catch (e) {
				response.data["message"] = "Campaign Created but not saved in db";
			}
			try {
				await this.sendSlackMessage(body);
			} catch (e) {}
			return CleverTap.createResponse(
				httpStatus.OK,
				contentType.ALL,
				response.data
			);
		}
		return CleverTap.createResponse(
			httpStatus.INTERNAL_ERROR,
			contentType.ALL,
			error.getInternalError()
		);
	}
}
