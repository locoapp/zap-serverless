export interface ContestBody {
	answer_cooldown_time: number;
	contest_category_name: string;
	contest_type_uid: string;
	created_at: string;
	date: number;
	is_active: boolean;
	is_dry_run: boolean;
	is_feedback_enabled: boolean;
	is_live_host: boolean;
	is_prize_distributed: boolean;
	language_cohort: any;
	name: string;
	prize: number;
	question_interval_time: number;
	question_time: number;
	show_time: number;
	start_time: number;
	status_freeze_time: number;
	stream_start_at: number;
	title: string;
	updated_at: string;
	user: number;
	uid: string | undefined;
}
export interface ContestMap {
	uid: string;
	show_time: number;
	category: string;
}
export interface Defaults {
	table_name: string;
	uid: string;
}

export interface AdHocCoinsBody {
	username: string;
	amount: number;
}
