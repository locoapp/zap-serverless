import { Handler, Context } from "aws-lambda";
import { dynamodb, converter } from "../aws";
import CleverTap from "../clevertap/clevertap";
import { getTableName } from "../tableNames";
import httpStatus = require("../utils/httpStatus");
import contentType = require("../utils/contentType");
import error = require("../utils/error");
import axios from "axios";
import config = require("../utils/config");
import { ContestBody, ContestMap } from "../contest/contestInterface";
import { Body } from "../clevertap/CreateInterface";
import dayjs = require("dayjs");

const scheduleCampaigns: Handler = async (event: any, context: Context) => {
	const env = process.env["STAGE"] || "dev";
	const url: string =
		env === "dev" ? config.contestUrl.devURL : config.contestUrl.prodURL;
	let allContests: ContestBody[] = await getAllContests(url).then(
		response => response.data.data
	);
	let contestUidTimeMap: ContestMap[] = currentContest(allContests);
	console.log(contestUidTimeMap);
	if (contestUidTimeMap.length == 0) return;
	let allContestCampaigns = await getAllContestCampaigns(contestUidTimeMap[0]);
	if (allContestCampaigns.length == 0) {
		return createCampaign(contestUidTimeMap[0]);
	}
	return CleverTap.createResponse(
		httpStatus.BAD_REQUEST,
		contentType.ALL,
		error.getBadError()
	);
};

const createCampaign = async contestMap => {
	const CLEVERTAP_ACCOUNT_ID =
		process.env.CLEVERTAP_ACCOUNT_ID || "TEST-86Z-KW7-5K5Z";
	const CLEVERTAP_ACCOUNT_PASSCODE =
		process.env.CLEVERTAP_ACCOUNT_PASSCODE || "SMK-RKX-ULKL";
	let defaultNotifications = await getDefaultNotifications(contestMap.category);
	let currentNotification =
		defaultNotifications[
			Math.floor(Math.random() * defaultNotifications.length)
		];
	let payload: Body = createCampaignPayload(contestMap, currentNotification);

	const clevertap = new CleverTap(
		CLEVERTAP_ACCOUNT_ID,
		CLEVERTAP_ACCOUNT_PASSCODE
	);
	try {
		const response = await clevertap.Targets("CREATE", payload);
		console.log(response);
		return response;
	} catch (e) {}
};
const getDefaultNotifications = async (category: string) => {
	let table = getTableName("default_notifications");
	const params = {
		TableName: table
	};
	params["FilterExpression"] = "#category = :c";
	params["ExpressionAttributeNames"] = { "#category": "category" };
	params["ExpressionAttributeValues"] = {
		":c": {
			S: category
		}
	};
	let response = await dynamodb.scan(params).promise();
	//unmarshall is static method
	var unmarshalled = response.Items.map(item => converter.unmarshall(item));
	return unmarshalled;
};
const createCampaignPayload = (contestMap: ContestMap, notification) => {
	let show_time = dayjs(contestMap.show_time - 300000);
	let when = show_time.format("YYYYMMDD HH:mm");
	let dateString = show_time.format("DD MMM");
	let time = show_time.format("HH:mm"); // before 5 minutes
	let payload: Body = {
		name: `All-${dateString}-${contestMap.category}-${time}`,
		when: when,
		content: {
			title: notification.title,
			subject: notification.title,
			body: notification.message,
			sender_name: "Loco",
			platform_specific: {
				android: {
					type: "clevertap",
					title: notification.title,
					message: notification.message
				}
			}
		},
		target_mode: "push",
		where: {
			common_profile_properties: {
				profile_fields: []
			}
		},
		devices: ["ios", "android"],
		uid: contestMap.uid
	};
	return payload;
};

const getAllContestCampaigns = async body => {
	let table = getTableName("clevertap_campaigns");
	const params = {
		TableName: table
	};
	params["FilterExpression"] = "#uid = :u";
	params["ExpressionAttributeNames"] = { "#uid": "uid" };
	params["ExpressionAttributeValues"] = {
		":u": {
			S: body.uid
		}
	};
	let response = await dynamodb.scan(params).promise();
	//unmarshall is static method
	var unmarshalled = response.Items.map(item => converter.unmarshall(item));
	return unmarshalled;
};

const currentContest = (allContests: ContestBody[]) => {
	return allContests
		.filter(
			contest =>
				new Date() >= new Date(contest.show_time - 1800000) &&
				new Date() <= new Date(contest.show_time)
		) //will check if scheduled contest is after 30minutes
		.sort((a, b) => a.show_time - b.show_time) // though it would only be one contests, sorting anyways
		.map(contest => ({
			uid: contest.uid,
			show_time: contest.show_time,
			category: contest.contest_category_name
		}));
};

const getAllContests = async (url: string) => {
	let retry = 0;
	let maxTries = 3;
	let response;
	while (true) {
		try {
			response = await axios.get(url + "all_contests", config.Headers);
			break;
		} catch (e) {
			if (++retry == maxTries) {
				response = [];
			}
		}
	}
	return response;
};

export { scheduleCampaigns };
