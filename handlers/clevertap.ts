import httpStatus = require('../utils/httpStatus')
import contentType = require('../utils/contentType')
import error = require('../utils/error')
import { Handler, Context } from "aws-lambda";
import CleverTap from '../clevertap/clevertap'
import { Body, StopBody } from '../clevertap/CreateInterface'


const createCampaign: Handler = async (event: any, context: Context) => {
  const CLEVERTAP_ACCOUNT_ID = process.env.CLEVERTAP_ACCOUNT_ID || "TEST-86Z-KW7-5K5Z"
  const CLEVERTAP_ACCOUNT_PASSCODE = process.env.CLEVERTAP_ACCOUNT_PASSCODE || "SMK-RKX-ULKL"

  if (event.body != undefined) {
    let body: Body = JSON.parse(event.body)
    if(body.content.title.includes("Enter the title ") || body.content.subject.includes("Enter the message")){
      return CleverTap.createResponse(httpStatus.BAD_REQUEST, contentType.ALL, error.notValidBody())
    }
    const clevertap = new CleverTap(CLEVERTAP_ACCOUNT_ID, CLEVERTAP_ACCOUNT_PASSCODE)
    const response = await clevertap.Targets('CREATE', body)
    return response
  }

  return CleverTap.createResponse(httpStatus.BAD_REQUEST, contentType.ALL, error.getBadError())
}
const stopCampaign: Handler = async (event: any, context: Context) => {
  const CLEVERTAP_ACCOUNT_ID = process.env.CLEVERTAP_ACCOUNT_ID || "TEST-86Z-KW7-5K5Z"
  const CLEVERTAP_ACCOUNT_PASSCODE = process.env.CLEVERTAP_ACCOUNT_PASSCODE || "SMK-RKX-ULKL"

  if (event.body != undefined) {
    let body: StopBody = JSON.parse(event.body)
    const clevertap = new CleverTap(CLEVERTAP_ACCOUNT_ID, CLEVERTAP_ACCOUNT_PASSCODE)
    const response = await clevertap.Targets('STOP', body)
    return response
  }

  return CleverTap.createResponse(httpStatus.BAD_REQUEST, contentType.ALL, error.getBadError())

}


export { createCampaign, stopCampaign }