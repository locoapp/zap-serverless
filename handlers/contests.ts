import httpStatus = require("../utils/httpStatus");
import contentType = require("../utils/contentType");
import createResponse = require("../utils/response");
import error = require("../utils/error");
import { Handler, Context } from "aws-lambda";
import { dynamodb, converter } from "../aws";
import axios from "axios";
import config = require("../utils/config");
import {
	Defaults,
	ContestBody,
	AdHocCoinsBody
} from "../contest/contestInterface";

const contestDefaults: Handler = async (event: any, context: Context) => {
	if (event.body != undefined) {
		let body: Defaults = JSON.parse(event.body);
		let table = body.table_name;
		const params = {
			TableName: table
		};
		if (body.uid !== undefined && body.uid !== null) {
			(params["FilterExpression"] = "#uid = :u"),
				(params["ExpressionAttributeNames"] = { "#uid": "uid" }),
				(params["ExpressionAttributeValues"] = {
					":u": {
						S: body.uid
					}
				});
		}
		let response = await dynamodb.scan(params).promise();
		//unmarshall is static method
		var unmarshalled = response.Items.map(item => converter.unmarshall(item));
		return createResponse(httpStatus.OK, contentType.ALL, unmarshalled);
	}

	return createResponse(
		httpStatus.BAD_REQUEST,
		contentType.ALL,
		error.getBadError()
	);
};

const createContest: Handler = async (event: any, context: Context) => {
	if (event.body != undefined) {
		const env = process.env["STAGE"] || "dev";
		const url: string =
			env === "dev" ? config.contestUrl.devURL : config.contestUrl.prodURL;
		let body: ContestBody = JSON.parse(event.body);
		try {
			let response = await axios.post(
				url + "create_contest/",
				body,
				config.Headers
			);
			return createResponse(httpStatus.OK, contentType.ALL, response.data);
		} catch (e) {
			return createResponse(httpStatus.BAD_REQUEST, contentType.ALL, e.message);
		}
	}

	return createResponse(
		httpStatus.BAD_REQUEST,
		contentType.ALL,
		error.getBadError()
	);
};

const allContests: Handler = async (event: any, context: Context) => {
	const env = process.env["STAGE"] || "dev";
	const url: string =
		env === "dev" ? config.contestUrl.devURL : config.contestUrl.prodURL;
	try {
		let response = await axios.get(url + "all_contests", config.Headers);
		return createResponse(httpStatus.OK, contentType.ALL, response.data);
	} catch (e) {
		return createResponse(httpStatus.BAD_REQUEST, contentType.ALL, e.message);
	}
};

const allContestForeigns: Handler = async (event: any, context: Context) => {
	const env = process.env["STAGE"] || "dev";
	const url: string =
		env === "dev" ? config.contestUrl.devURL : config.contestUrl.prodURL;
	try {
		let response = await axios.get(url + "all_contest_foreign", config.Headers);
		return createResponse(httpStatus.OK, contentType.ALL, response.data);
	} catch (e) {
		return createResponse(httpStatus.BAD_REQUEST, contentType.ALL, e.message);
	}
};

const grantAdhocCoins: Handler = async (event: any, context: Context) => {
	const env = process.env["STAGE"] || "dev";
	const url: string =
		env === "dev" ? config.contestUrl.devURL : config.contestUrl.prodURL;
	let body: AdHocCoinsBody = JSON.parse(event.body);
	try {
		let response = await axios.post(url + "adhoc_coins/", body, config.Headers);
		return createResponse(httpStatus.OK, contentType.ALL, response.data);
	} catch (e) {
		return createResponse(
			httpStatus.BAD_REQUEST,
			contentType.ALL,
			e.response.data
		);
	}
};

export {
	contestDefaults,
	createContest,
	allContests,
	allContestForeigns,
	grantAdhocCoins
};
