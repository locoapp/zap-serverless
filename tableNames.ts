function getTableName (tableName) {
    const env = process.env['STAGE'] || 'dev'
    return env + '_' + tableName
  }
  
  export  {
    getTableName
  }
  