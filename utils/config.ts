export = {
    contestUrl : {
      devURL: "http://dev.getloconow.com/v2/internal_service/",
      prodURL: "http://host.getloconow.com/v2/internal_service/"

    },
    slack_webhook_url: {
        devURL:"https://hooks.slack.com/services/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",
        prodURL: "https://hooks.slack.com/services/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"
    },
    Headers:{
     headers : {
        'CLIENT-ID':process.env['CLIENT_ID'],
        'CLIENT-SECRET':process.env['CLIENT_SECRET'],
        'Content-Type':'application/json'
        }
    }
};

