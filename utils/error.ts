function getInternalError () {
  return {
    'message': 'Internal Error occure, please try again!',
    'error_code': 'SL000'
  }
}
function getNotValidError(){
  return {
    'message': 'Not Valid Target api endpoint ',
    'error_code': 'TL000'
  }
}
function getBadError(){
  return {
    'message': 'The request could not be satisfied',
    'error_code': 'TL404'
  }
}
function notValidBody(){
  return {
    'message': 'Title or Message are not accepted',
    'error_code': 'TL4201'
  }
}

export = {
  'getInternalError': getInternalError,
   'getNotValidError': getNotValidError,
   'getBadError': getBadError,
   'notValidBody':notValidBody
}
