function createResponse(statusCode, headers, message) {
    return {
      statusCode: statusCode,
      body: JSON.stringify(message),
      headers:headers
    }
  }

  export = createResponse